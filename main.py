class Pet(object):
    species = ['dog', ' cat', 'horse', 'hamster']

    def __init__(self, name="", species=None):
        self.name = name
        self.species = species


    def getName(self):
        return self.name

    def getSpecies(self):
        return self.species

    def __str__(self):
        return "Species of:%s, named %s",  (self.species, self.name)


class Dog(Pet):

    def __init__(self, name="", chases_cats):
        Pet.__init__(self, name, "Dog")
        self.chases_cats = chases_cats

    def chasesCats(self):
        return self.chases_cats

    def __str__(self):
        return "Species of:Dog, named %s",  ( self.name)

class Cat(Pet):
    def __init__(self, name="", hates_dogs):
        Pet.__init__(self, name, "Cat")
        self.hates_dogs = hates_dogs

    def hatesDogs(self):
        return self.hates_dogs

    def __str__(self):
        return "Species of:Cat, named %s", (self.name)

def whichone(petlist, name):
    for pet in petlist:
        if pet.name == name:
            return pet
    return None

def main():
    animals = []

    choices = """
            enter the species
            1.dog
            2.cat
            3.horse
            4.hamster

            Choice: """
    feedback = ""
    while True:

        if len(feedback) > 0:
            #choice = feedback[0]
            spe = whichone(species, feedback)
            if not spe:
                print("Enter a proper species. Please try again.\n")
            else:
                pet.species = feedback

